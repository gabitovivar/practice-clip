//Ejecutamos el query fotos
const db = require("../server/db");
module.exports = {
  find: (req, res) => {
    const sql = "select * from photos;";
    db.query(sql, (err, result, fiels) => {
      if (err) {
        res.status(400).json({
          code: "SQL_ERROR"
        });
        return;
      }
      res.status(200).json(result);
    });
  }
};
