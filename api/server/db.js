//instancia de la bd
const mysql = require("mysql");
const conn = mysql.createConnection({
  host: "dbmysql",
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  multipleStatements: true
});

module.exports = conn;
