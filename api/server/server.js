const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  methodOverride = require("method-override"),
  migrate = require("./init_db");

let router = express.Router();

const forumController = require("../controllers/forums");
const photosController = require("../controllers/photos");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

migrate.init();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

router.route("/forums").get(forumController.find);
router.route("/photos").get(photosController.find);

app.use("/api", router);
app.listen(9000, () => {
  console.log("The node.js app is listening on port 9000.");
});
