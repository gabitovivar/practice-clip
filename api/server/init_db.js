//Queries para crear las tablas de la bd
const db = require("./db");
function migrate() {
  const sql = `CREATE TABLE IF NOT EXISTS forums (
        id int NOT NULL auto_increment,
        title text,
        link varchar(255) NOT NULL UNIQUE,
        media text,
        date_taken varchar(255),
        description text,
        published datetime,
        author varchar(255) ,
        author_id varchar(255) ,
        tags text,
        PRIMARY KEY (id)
     )
     CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;
    
    
    CREATE TABLE IF NOT EXISTS photos (
        id int NOT NULL auto_increment,
        title text,
        link varchar(255) NOT NULL UNIQUE,
        media text,
        date_taken varchar(255),
        description text,
        published datetime ,
        author varchar(255) ,
        author_id varchar(255),
        tags text,
        PRIMARY KEY (id)
     )
     CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;`;
  db.query(sql, (err, res) => {
    if (err) {
      console.log(err);
      return;
    }
    console.log("tables created");
  });
}
module.exports = {
  init: () => {
    migrate();
  }
};
