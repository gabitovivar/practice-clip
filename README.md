# Getting started

Running a nodejs application with mysql database and reactjs SPA using docker

- Launch mysql server in a docker container.
- Launch our simple node app in a separate container.
- Launch our Web app with Reactjs

## Prerequisites

- must have docker set up and running on your system
- must have git set up and runnint on your system

## Installation

```
 git clone https://gitlab.com/gabitovivar/practice-clip.git`
 cd practice-clip
 docker-compose build
 docker-compose up
 ```

## LINKS

#### web page
 [localhost:9002](http://localhost:9022)
#### api
 [localhost:9000/api/forums](http://localhost:9000/api/forums)
 [localhost:9000/api/photos](http://localhost:9000/api/photos)

## Why is it good?
* The strong point in this application is data insertion. All duplicate data validation is made right in the database instead of checking it manually in the code, which would have taken a lot more time because node is not intended for heavy workloads
What are the drawbacks given the constraints or other factors?

## What are the drawbacks given the constraints or other factors?
* The back end is not scalable because it is not mounted in microservices

## What would you change and/or improve?
* In order to make it easier to scale, I would make one container hosting the cronjob to run every n minutes to fetch and parse data obtained from the webservice.
* Then, in another container I would set a couple of web hooks to receive the parsed data and insert it to the database.
* This infraestructure allows to make an auto-scalable group coupled with a load balancer in case the number of data increases.
* Implement a library that creates migrations for the database




