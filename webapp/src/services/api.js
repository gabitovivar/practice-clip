import axios from "axios";
//Se crea la instancia de axios para hacer los request al api
export const api = axios.create({
  baseURL: "http://localhost:9000/api/"
});
