import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import { Home, Forums, Photos } from "./components";
import "bootstrap/dist/css/bootstrap.min.css";
// Utilice bootstrap para el diseño
//Y se utilizo react-router-dom para la navegacion
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path={"/"} exact component={Home} />
          <Route path={"/foros"} exact component={Forums} />
          <Route path={"/fotos"} exact component={Photos} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
