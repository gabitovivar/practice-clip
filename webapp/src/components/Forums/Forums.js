import React, { Component } from "react";
import { Card, Spinner } from "react-bootstrap";
import moment from "moment";
import { api } from "../../services/api";
import "./forums.css";
class Forums extends Component {
  state = {
    forums: [],
    loading: true,
    error: ""
  };

  //Una vez que renderea el componente ejecutamos el request para obtener los valores del api
  async componentDidMount() {
    try {
      this.setState({
        loading: true
      });
      let { data } = await api.get(`/forums`);
      this.setState({
        forums: data,
        loading: false
      });
    } catch (e) {
      this.setState({
        forums: [],
        loading: false,
        error: "Ocurrion un error intente mas tarde"
      });
    }
  }

  //rendereamos la informacion
  render() {
    const { forums, loading } = this.state;
    return loading ? (
      <div className="photos-loading">
        <Spinner animation="grow" variant="dark" />
      </div>
    ) : (
      <div className="container-forums">
        {forums.map((i, index) => {
          return (
            <Card className="container-forums__card">
              <Card.Header>
                <a href={i.link}>{i.title}</a>
              </Card.Header>
              <Card.Body>
                <blockquote className="blockquote mb-0">
                  <p>{i.description}</p>
                  <footer className="blockquote-footer">
                    {i.author} -
                    <cite title="Source Title">
                      {moment(i.published).format("DD/MM/YYYY HH:mm:ss ")}
                    </cite>
                  </footer>
                </blockquote>
              </Card.Body>
            </Card>
          );
        })}
      </div>
    );
  }
}

export default Forums;
