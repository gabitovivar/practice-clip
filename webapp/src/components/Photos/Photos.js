import React, { Component } from "react";
import { Image, Spinner } from "react-bootstrap";
import { api } from "../../services/api";
import "./photos.css";
class Photos extends Component {
  state = {
    photos: [],
    loading: true,
    error: ""
  };
  //Una vez que renderea el componente ejecutamos el request para obtener los valores del api
  async componentDidMount() {
    try {
      this.setState({
        loading: true
      });
      let { data } = await api.get(`/photos`);
      this.setState({
        photos: data,
        loading: false
      });
    } catch (e) {
      this.setState({
        photos: [],
        loading: false,
        error: "Ocurrio un error intente mas tarde"
      });
    }
  }
  render() {
    const { photos, loading } = this.state;
    return loading ? (
      <div className="photos-loading">
        <Spinner animation="grow" variant="dark" />
      </div>
    ) : (
      <div className="container-photos">
        {photos.map((i, index) => {
          let media = JSON.parse(i.media);
          return <Image src={media.m} fluid />;
        })}
      </div>
    );
  }
}

export default Photos;
