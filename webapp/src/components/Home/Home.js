import React, { Component } from "react";
import { Button } from "react-bootstrap";
import "./home.css";
class Home extends Component {
  // compoenente para navegar por el webapp
  render() {
    const { history } = this.props;
    return (
      <div className="container-button">
        <Button
          variant="primary"
          size="lg"
          onClick={() => history.push("/foros")}
          block
        >
          Foros
        </Button>
        <Button
          variant="secondary"
          size="lg"
          onClick={() => history.push("/fotos")}
          block
        >
          Fotos
        </Button>
      </div>
    );
  }
}

export default Home;
