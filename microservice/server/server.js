const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  methodOverride = require("method-override"),
  cron = require("../services/crons");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
cron.init();
app.listen(9001, () => {
  console.log("The node.js app is listening on port 9001.");
});
