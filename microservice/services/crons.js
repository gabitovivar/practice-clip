const request = require("request"),
  schedule = require("node-schedule"),
  db = require("../server/db");
mysql = require("mysql");

//funcion que ejecuta el request para obtener los foros de flickr
function loadForums() {
  request.get(
    "https://www.flickr.com/services/feeds/forums.gne?format=json&lang=es-us&nojsoncallback=1",
    async (error, response, body) => {
      if (error) {
        console.log(error);
        return;
      }
      // convertimos la repuesta en objeto
      body = JSON.parse(body);
      // Obtenemos solo los items de la respuesta
      const { items } = body;
      let values = [];
      //Query para el insert de los datos, se utiliza el on duplicate key update para actualice los datos que existan como primary key o unique ,
      let sql = `INSERT INTO forums(
        title,
        link,
        media,
        date_taken,
        description,
        published,
        author,
        author_id,
        tags) 
        VALUES ? ON DUPLICATE KEY UPDATE
        title=VALUES(title),
        link=VALUES(link),
        media=VALUES(media),
        date_taken=VALUES(date_taken),
        description=VALUES(description),
        published=VALUES(published),
        author=VALUES(author),
        author_id=VALUES(author_id),
        tags=VALUES(tags)`;

      // Aqui creo los valores para la consulta
      items.forEach(i => {
        const {
          title,
          link,
          media,
          date_taken,
          description,
          published,
          author,
          author_id,
          tags
        } = i;

        values.push([
          title,
          link,
          JSON.stringify(media),
          date_taken,
          description,
          new Date(published),
          author,
          author_id,
          tags
        ]);
      });
      // Formateamos la query con los valores
      sql = mysql.format(sql, [values]);

      //Ejecuto el query
      db.query(sql, (errors, result) => {
        if (errors) {
          console.log(errors);
          return;
        }
        console.log("OK FORUMS ", result);
      });
    }
  );
}

//funcion que ejecuta el request para obtener los foros de flickr
function loadPhotos() {
  request.get(
    "https://www.flickr.com/services/feeds/photos_public.gne?format=json&lang=es-us&nojsoncallback=1",
    async (error, response, body) => {
      if (error) {
        console.log(error);
        return;
      }
      // convertimos la repuesta en objeto
      body = JSON.parse(body);
      // Obtenemos solo los items de la respuesta
      const { items } = body;
      let values = [];
      //Query para el insert de los datos, se utiliza el on duplicate key update para actualice los datos que existan como primary key o unique ,
      let sql = `INSERT INTO photos(
        title,
        link,
        media,
        date_taken,
        description,
        published,
        author,
        author_id,
        tags) 
        VALUES ? ON DUPLICATE KEY UPDATE
        title=VALUES(title),
        link=VALUES(link),
        media=VALUES(media),
        date_taken=VALUES(date_taken),
        description=VALUES(description),
        published=VALUES(published),
        author=VALUES(author),
        author_id=VALUES(author_id),
        tags=VALUES(tags)`;
      // Aqui creo los valores para la consulta
      items.forEach(i => {
        const {
          title,
          link,
          media,
          date_taken,
          description,
          published,
          author,
          author_id,
          tags
        } = i;

        values.push([
          title,
          link,
          JSON.stringify(media),
          date_taken,
          description,
          new Date(published),
          author,
          author_id,
          tags
        ]);
      });
      // Formateamos la query con los valores
      sql = mysql.format(sql, [values]);

      //Ejecuto el query
      db.query(sql, (errors, result) => {
        if (errors) {
          console.log(errors);
          return;
        }
        console.log("OK PHOTOS ", result);
      });
    }
  );
}
//exportamos la funcion init para aqui donde llamamos las funciones privadas y creamos un cron job para que se esten actualizando cada minuto
module.exports = {
  init: () => {
    loadForums();
    loadPhotos();
    schedule.scheduleJob("*/1 * * * *", function() {
      console.log("cron job is running");
      loadForums();
      loadPhotos();
    });
  }
};
